﻿namespace AIdle {
    [System.Serializable]
    public class LocaleData {
        public string id;
        public string ru;
        public string en;
    }

    [System.Serializable]
    public class LocaleDataCollection {
        public LocaleData[] elements;
    }
}
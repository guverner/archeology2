﻿namespace AIdle {
    [System.Serializable]
    public class TileInfo {
        public enum TileType { GRASS, EARTH, CHEST, STONE };

        #region Properties
        public int currentHealth;
        public int maxHealth;
        public TileType type;
        #endregion

        public TileInfo(int currentHealth, int maxHealth, TileType type) {
            this.currentHealth = currentHealth;
            this.maxHealth = maxHealth;
            this.type = type;
        }

        public void RemoveHealth(int damage) {
            currentHealth -= damage;
            if (currentHealth < 0)
                currentHealth = 0;
        }
    }
}
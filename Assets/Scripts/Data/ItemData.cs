﻿namespace AIdle {
    [System.Serializable]
    public class ItemData {
        public string id;
        public string name;
        public string superitem;
        public int price;
        public string[] subitems;
    }

    [System.Serializable]
    public class ItemDataCollection {
        public ItemData[] elements;
    }
}
﻿using System.Collections.Generic;
using System.Text;

namespace AIdle {
    [System.Serializable]
    public class SaveData {
        public int coins;
        public int pickaxeLevel;
        public int incomeLevel;
        public List<string> itemsPlayerHaveIDs;
        public TileInfo[] tilesInfo;

        public string ToDebugString() {
            StringBuilder strBuilder = new StringBuilder("SaveData:\n");

            strBuilder.AppendLine("coins=" + coins);
            strBuilder.AppendLine("pickaxeLevel=" + pickaxeLevel);
            strBuilder.AppendLine("incomeLevel=" + incomeLevel);

            strBuilder.AppendLine("itemsPlayerHaveIDs=");
            foreach (string itemID in itemsPlayerHaveIDs)
                strBuilder.AppendLine(itemID);

            strBuilder.AppendLine("tilesInfo=");
            if(tilesInfo != null) {
                //output row by row
                int row = 0;
                int column = 0;
                foreach (TileInfo tileInfo in tilesInfo) {
                    strBuilder.AppendFormat("row={0} column={1} health={2}/{3} type={4}\n",
                        row, column, tileInfo.currentHealth, tileInfo.maxHealth, tileInfo.type);
                }
            }

            return strBuilder.ToString();
        }

        private string ListToString<T>(List<T> list, char delimiter, int numElementsInRow) {
            StringBuilder strBuilder = new StringBuilder("");

            if(list != null && list.Count > 0) {
                int i = 0;
                foreach(var item in list) {
                    strBuilder.AppendFormat("{0, 4}" + delimiter, item.ToString());
                    i++;
                    if (numElementsInRow > 0 && i % numElementsInRow == 0)
                        strBuilder.Append('\n');
                }
            }

            return strBuilder.ToString();
        }
    }
}
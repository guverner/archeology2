﻿using UnityEngine;
using System.Collections;

namespace AIdle {
    //Dont create objects from this class!
    public class UpgradableTool {
        #region Properties
        public int Level { get; protected set; }
        public float Value { get; protected set; }
        #endregion

        public virtual void Upgrade() {
            Level++;
            Value = NextValue();
        }

        #region Need Override
        public virtual float NextValue() { return 0.0f; }
        public virtual int Price() { return 0; }
        public virtual bool Buy() { return false; }
        #endregion
    }
}
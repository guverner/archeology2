﻿using UnityEngine;
using System.Collections;

namespace AIdle {
    public class PassiveIncomeTool : UpgradableTool {
        public PassiveIncomeTool(int level) {
            Level = level;
            Value = NextValue();
        }

        public override float NextValue() {
            return Level;
        }
        public override int Price() {
            return Level * 15 + 30;
        }
        public override bool Buy() {
            return ResourcesManager.Instance.BuyPassiveIncomeUpgrade();
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System;

namespace AIdle {
    public class PickaxeTool : UpgradableTool {
        public PickaxeTool(int level) {
            Level = level;
            Value = NextValue();
        }

        public override float NextValue() {
            return Level + 1.0f;
        }
        public override int Price() {
            return Level * 10 + 20;
        }
        public override bool Buy() {
            return ResourcesManager.Instance.BuyPickaxeUpgrade();
        }
    }
}
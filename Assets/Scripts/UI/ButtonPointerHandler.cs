﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace AIdle {
    [RequireComponent(typeof(CanvasGroup))]
    public class ButtonPointerHandler : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler,
        IPointerExitHandler, IPointerEnterHandler {

        #region Fields
        [SerializeField]
        private Transform m_objToScale;
        [SerializeField]
        private float m_onDownScaleSize = 0.9f;//in percents
        [SerializeField]
        private CanvasGroup m_canvasGroup;
        private bool m_isDown;
        private UnityAction m_onAfterClickCallback;
        #endregion

        #region Public
        public void SetAfterClickCallback(UnityAction onAfterClickCallback) {
            m_onAfterClickCallback = onAfterClickCallback;
        }
        #endregion

        #region Properties
        protected bool IsBlocked {
            get { return !m_canvasGroup.blocksRaycasts; }
            set { m_canvasGroup.blocksRaycasts = !value; }
        }
        #endregion

        #region Pointer Handlers
        public void OnPointerClick(PointerEventData eventData) {
            OnAfterClick();
        }

        public void OnPointerDown(PointerEventData eventData) {
            m_isDown = true;
            MakeDownSize();
            OnDown();
        }

        public void OnPointerUp(PointerEventData eventData) {
            if (!m_isDown) return;

            m_isDown = false;
            ResetSize();
            OnUp();
        }

        public void OnPointerEnter(PointerEventData eventData) {
            if (!m_isDown) return;

            MakeDownSize();
        }

        public void OnPointerExit(PointerEventData eventData) {
            if (!m_isDown) return;

            ResetSize();
        }
        #endregion

        #region Virtual
        protected virtual void OnAfterClick() {
            if (m_onAfterClickCallback != null)
                m_onAfterClickCallback();
        }

        protected virtual void OnDown() {
        }

        protected virtual void OnUp() {
        }
        #endregion

        #region Helpers
        private void ResetSize() {
            m_objToScale.localScale = Vector3.one;
        }

        private void MakeDownSize() {
            Vector3 newSize = new Vector3(m_onDownScaleSize, m_onDownScaleSize, 1.0f);
            if (newSize != m_objToScale.localScale)
                m_objToScale.localScale = newSize;
        }
        #endregion
    }
}
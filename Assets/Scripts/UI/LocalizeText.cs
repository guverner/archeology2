﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AIdle {
    public class LocalizeText : MonoBehaviour {
        void Start() {
            Text text = this.GetComponent<Text>();
            if(text != null)
                text.text = LocaleManager.Instance.Text(text.text);
        }
    }
}
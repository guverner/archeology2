﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace AIdle {
    public class ItemsManager : UnitySingleton<ItemsManager> {
        #region Fields
        [SerializeField]
        private TextAsset m_itemsDataBinaryFile;
        private Dictionary<string, ItemData> m_itemsDataDict;

        private List<string> m_itemsPlayerHave;
        private List<string> m_itemsCanFall;//common and subitems. Item is removed when its taken
        #endregion

        #region Properties
        public int NumItemsTotal { get { return m_itemsDataDict.Count; } }
        public List<string> ItemsPlayerHave { get { return m_itemsPlayerHave; } }
        #endregion

        #region Initialisation
        public void StartUp() {
            m_itemsDataDict = LoadItemsDataDict(m_itemsDataBinaryFile);
            m_itemsDataBinaryFile = null;

            m_itemsPlayerHave = new List<string>();
            if (PersistenceManager.Instance.SavedData != null)
                m_itemsPlayerHave.AddRange(PersistenceManager.Instance.SavedData.itemsPlayerHaveIDs);

            m_itemsCanFall = GetItemsIdsCanFall(m_itemsDataDict, m_itemsPlayerHave);
        }
        #endregion

        #region Public
        public ItemData GetItem(string itemID) {
            return m_itemsDataDict[itemID];
        }

        //0 - random item, 1 - super item (if all subitems collected)
        public ItemData[] AddRandomItem() {
            ItemData[] addedItems = new ItemData[2];

            if(m_itemsCanFall.Count == 0)
                return addedItems;

            string randomItemName = m_itemsCanFall[Random.Range(0, m_itemsCanFall.Count)];
            
            m_itemsPlayerHave.Add(randomItemName);
            m_itemsCanFall.Remove(randomItemName);

            ItemData itemData = m_itemsDataDict[randomItemName];
            addedItems[0] = itemData;

            if (itemData.superitem != null) {
                ItemData superitem = m_itemsDataDict[itemData.superitem];

                bool isPlayerHasAllSubitems = true;

                foreach(string subitemName in superitem.subitems) {
                    if(!m_itemsPlayerHave.Contains(subitemName)) {
                        isPlayerHasAllSubitems = false;
                        break;
                    }
                }

                if(isPlayerHasAllSubitems) {
                    m_itemsPlayerHave.Add(superitem.id);
                    addedItems[1] = superitem;
                }
            }

            PersistenceManager.Instance.Save();

            return addedItems;
        }

        public int GetNumItemsCanFall() {
            return m_itemsCanFall.Count;
        }
        #endregion

        #region Helpers
        private Dictionary<string, ItemData> LoadItemsDataDict(TextAsset itemsDataBinaryFile) {
            ItemDataCollection itemDataCollection = BinaryFileManager.Load<ItemDataCollection>(itemsDataBinaryFile);

            Dictionary<string, ItemData> tempDict = new Dictionary<string, ItemData>();

            foreach(ItemData item in itemDataCollection.elements)
                tempDict.Add(item.id, item);

            return tempDict;
        }

        private List<string> GetItemsIdsCanFall(Dictionary<string, ItemData> itemsDataDict, List<string> itemsPlayerHave) {
            List<string> tempList = new List<string>();

            foreach(var item in itemsDataDict) {
                if(item.Value.subitems == null || item.Value.subitems.Length == 0)
                    tempList.Add(item.Key);
            }

            if(itemsPlayerHave != null) {
                foreach(string itemId in itemsPlayerHave) {
                    tempList.Remove(itemId);
                }
            }

            return tempList;
        }
        #endregion
    }
}
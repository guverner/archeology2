﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace AIdle {
    public enum GameState {
        NONE,
        SETUP,
        MENU,
        GAME
    };

    public class GameManager : UnitySingleton<GameManager> {
        #region Fields
        [SerializeField]
        private LoadingScreen m_loadingScreen;
        #endregion

        #region Properties
        public GameState CurrentState { get; private set; }
        #endregion

        #region Initialisation
        protected override void Init() {
            CurrentState = GameState.SETUP;
            DOTween.Init(true);

            SceneManager.sceneLoaded += OnSceneLoaded;

            m_loadingScreen.Init(OnLoadingScreenFadeComplete);
            m_loadingScreen.Show(false);

            PersistenceManager.Instance.StartUp();
            ItemsManager.Instance.StartUp();
            ResourcesManager.Instance.StartUp();

            m_loadingScreen.MakeAbleToHide();
        }
        #endregion

        #region Unity Events
        private void OnDestroy() {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode) {
            if(scene.name == Constants.SceneNames.MENU)
                CurrentState = GameState.MENU;
            else if(scene.name == Constants.SceneNames.GAME)
                CurrentState = GameState.GAME;

            m_loadingScreen.Hide();
        }
        #endregion

        #region Public
        public void LoadGameScene() {
            m_loadingScreen.Show(true);
        }
        #endregion

        #region Helpers
        private void OnLoadingScreenFadeComplete(bool isFadeInFinish) {
            if(isFadeInFinish) {
                if (CurrentState == GameState.SETUP)
                    SceneManager.LoadSceneAsync(Constants.SceneNames.MENU);
                else if (CurrentState == GameState.MENU)
                    SceneManager.LoadSceneAsync(Constants.SceneNames.GAME);
            }
            else {
                if (CurrentState == GameState.GAME)
                    GameObject.Destroy(m_loadingScreen.transform.parent.gameObject);
            }
        }
        #endregion
    }
}
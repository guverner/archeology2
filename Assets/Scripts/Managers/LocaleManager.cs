﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIdle {
    public class LocaleManager : UnitySingleton<LocaleManager> {
        #region Fields
        #region Inspector
        [SerializeField]
        private TextAsset m_localeDataBinaryFile;
        #endregion

        private Dictionary<string, string> _localeDict;//only 1 language
        #endregion

        protected override void Init() {
            LocaleDataCollection localeDataCollection = BinaryFileManager.Load<LocaleDataCollection>(m_localeDataBinaryFile);
            _localeDict = new Dictionary<string, string>(localeDataCollection.elements.Length);

            SystemLanguage currentlanguage = Application.systemLanguage;

            foreach (LocaleData localeData in localeDataCollection.elements) {
                if (currentlanguage == SystemLanguage.Russian)
                    _localeDict.Add(localeData.id, localeData.ru);
                else
                    _localeDict.Add(localeData.id, localeData.en);
            }

            m_localeDataBinaryFile = null;
        }

        public string Text(string id) {
            string value = null;
            if(_localeDict.TryGetValue(id, out value))
                return value;
            else
                return id;
        }
    }
}
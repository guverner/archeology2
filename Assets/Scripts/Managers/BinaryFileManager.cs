﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace AIdle {
    public class BinaryFileManager {
        private const string EXTENSION = ".bytes";

        #region Save
        public static void Save(IBinarySerializable data, string path, bool isPersistentDataPath = false,
            bool isForcePath = false) {

            Save(data.GetSaveData(), CreatePath(path, isForcePath, isPersistentDataPath));
        }

        public static void Save(object data, string path, bool isPersistentDataPath = false,
            bool isForcePath = false) {

            Save(data, CreatePath(path, isForcePath, isPersistentDataPath));
        }

        private static void Save(object data, string fullPath) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream stream = new FileStream(fullPath, FileMode.Create);
            bf.Serialize(stream, data);
            stream.Close();
        }
        #endregion

        #region Load
        public static T Load<T>(string path, bool isPersistentDataPath = false,
            bool isForcePath = false) where T : class {
            string fullPath = CreatePath(path, isForcePath, isPersistentDataPath);

            if (File.Exists(fullPath)) {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream stream = new FileStream(fullPath, FileMode.Open);
                T data = bf.Deserialize(stream) as T;
                stream.Close();
                return data;
            }
            return null;
        }

        public static void LoadTo(IBinarySerializable loadTo, string path, bool isPersistentDataPath = false,
            bool isForcePath = false) {
            string fullPath = CreatePath(path, isForcePath, isPersistentDataPath);

            if (File.Exists(fullPath)) {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream stream = new FileStream(fullPath, FileMode.Open);
                loadTo.UseData(bf.Deserialize(stream));
                stream.Close();
            }
        }

        public static T Load<T>(TextAsset binaryDataFile) where T : class {
            BinaryFormatter bf = new BinaryFormatter();
            Stream stream = new MemoryStream(binaryDataFile.bytes);
            T data = bf.Deserialize(stream) as T;
            stream.Close();
            return data;
        }

        public static void LoadTo(TextAsset binaryDataFile, IBinarySerializable loadTo) {
            BinaryFormatter bf = new BinaryFormatter();
            Stream stream = new MemoryStream(binaryDataFile.bytes);
            loadTo.UseData(bf.Deserialize(stream));
            stream.Close();
        }
        #endregion
        
        private static string CreatePath(string path, bool isForcePath, bool isPersistentDataPath) {
            System.Text.StringBuilder fullPathSB = new System.Text.StringBuilder();
            if(!isForcePath) {
                fullPathSB.Append(isPersistentDataPath == true ? Application.persistentDataPath : Application.dataPath);
                fullPathSB.Append("/");
            }
            fullPathSB.Append(path + EXTENSION);
            return fullPathSB.ToString();
        }
    }

    public interface IBinarySerializable {
        object GetSaveData();
        void UseData(object data);
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace AIdle {
    public class ResourcesManager : UnitySingleton<ResourcesManager> {
        #region Properties
        public float Coins { get; private set; }
        public UpgradableTool Pickaxe { get; private set; }
        public UpgradableTool PassiveIncome { get; private set; }
        public TileInfo[] TilesInfos { get; private set; }
        #endregion

        public event UnityAction<float, float> onCoinsChanged;//return: changedBy, total
        public event UnityAction<UpgradableTool> onUpgradePassiveIncome;

        #region Initialisation
        public void StartUp() {
            SaveData saveData = PersistenceManager.Instance.SavedData;

            if(saveData != null) {
                Coins = saveData.coins;
                Pickaxe = new PickaxeTool(saveData.pickaxeLevel);
                PassiveIncome = new PassiveIncomeTool(saveData.incomeLevel);
                TilesInfos = saveData.tilesInfo;
            }
            else {
                Pickaxe = new PickaxeTool(0);
                PassiveIncome = new PassiveIncomeTool(0);
            }
        }
        #endregion

        #region Coins
        //add or remove coins
        public void AddCoins(float numCoins) {
            if (numCoins == 0) return;

            Coins += numCoins;
            if (Coins < 0)
                Coins = 0;

            PersistenceManager.Instance.Save();

            if (onCoinsChanged != null)
                onCoinsChanged(numCoins, Coins);
        }

        public void AddCoinsByPassiveIncome() {
            AddCoins(PassiveIncome.Value);
        }
        #endregion

        #region Tiles
        public void SetTilesInfo(TileInfo[] tilesInfo) {
            TilesInfos = tilesInfo;
            PersistenceManager.Instance.Save();
        }
        #endregion

        #region Tools
        public bool BuyPickaxeUpgrade() {
            if(Coins >= (float)Pickaxe.Price()) {
                AddCoins(-Pickaxe.Price());
                Pickaxe.Upgrade();
                PersistenceManager.Instance.Save();
                return true;
            }
            return false;
        }

        public bool BuyPassiveIncomeUpgrade() {
            if (Coins >= (float)PassiveIncome.Price()) {
                AddCoins(-PassiveIncome.Price());
                PassiveIncome.Upgrade();
                PersistenceManager.Instance.Save();

                if (onUpgradePassiveIncome != null)
                    onUpgradePassiveIncome(PassiveIncome);

                return true;
            }
            return false;
        }
        #endregion
    }
}
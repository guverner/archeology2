﻿using UnityEngine;
using System.Collections.Generic;

namespace AIdle {
    public class PersistenceManager : UnitySingleton<PersistenceManager> {
        #region Properties
        public SaveData SavedData { get; private set; }
        #endregion

        #region Initialisation
        public void StartUp() {
            SavedData = BinaryFileManager.Load<SaveData>("saveData", true);

            if (SavedData == null)
                Debug.Log("[PersistenceManager] saveData not found");
            else
                Debug.Log(SavedData.ToDebugString());
        }
        #endregion

        #region Public
        public void Save() {
            if (SavedData == null)
                SavedData = new SaveData();

            SavedData.itemsPlayerHaveIDs = ItemsManager.Instance.ItemsPlayerHave;
            SavedData.coins = (int)ResourcesManager.Instance.Coins;
            SavedData.pickaxeLevel = ResourcesManager.Instance.Pickaxe.Level;
            SavedData.incomeLevel = ResourcesManager.Instance.PassiveIncome.Level;
            SavedData.tilesInfo = ResourcesManager.Instance.TilesInfos;

            BinaryFileManager.Save(SavedData, "saveData", true);
        }

        public bool IsMusicMuted {
            get { return PlayerPrefs.GetInt(Constants.PlayerPrefs.IS_MUSIC_MUTED, 0) == 0 ? false : true; }
            set { PlayerPrefs.SetInt(Constants.PlayerPrefs.IS_MUSIC_MUTED, value ? 1 : 0); }
        }

        public bool IsSfxMuted {
            get { return PlayerPrefs.GetInt(Constants.PlayerPrefs.IS_SFX_MUTED, 0) == 0 ? false : true; }
            set { PlayerPrefs.SetInt(Constants.PlayerPrefs.IS_SFX_MUTED, value ? 1 : 0); }
        }
        #endregion
    }
}
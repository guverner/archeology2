﻿using UnityEngine;
using System.Collections;

namespace AIdle {
    public enum SoundType { APPLAUSE, COIN, BUTTON };

    public class SoundManager : UnitySingleton<SoundManager> {
        #region Fields
        [SerializeField]
        private AudioClip m_applauseSound;
        [SerializeField]
        private AudioClip m_coinSound;
        [SerializeField]
        private AudioClip m_buttonSound;
        [SerializeField]
        private AudioClip m_shovelSound;

        public AudioSource musicSource;
        public int numEfxSources = 2;
        public int numEfxCoinsSources = 5;
        
        private AudioSource[] m_sfxSources;
        private AudioSource[] m_sfxCoinsSources;
        #endregion

        public bool IsMusicMuted { get; private set; }
        public bool IsSoundMuted { get; private set; }

        private const string COIN_SFX = "coin";
        private const string APPLAUSE_SFX = "applause";

        #region Initialisation
        protected override void Init() {
            m_sfxSources = InitAudioSources(numEfxSources);
            m_sfxCoinsSources = InitAudioSources(numEfxCoinsSources);

            SetMuteForMusic(PersistenceManager.Instance.IsMusicMuted);
            SetMuteForSfx(PersistenceManager.Instance.IsSfxMuted);
        }
        #endregion

        #region Public
        public void PlayApplauseSound() {
            PlaySingle(m_applauseSound, 0.5f);
        }

        public void PlayCoinSound() {
            PlaySingle(m_coinSound);
        }

        public void PlayButtonSound() {
            PlaySingle(m_buttonSound);
        }

        public void PlayShovelSound() {
            PlaySingle(m_shovelSound);
        }

        public void SetMuteForMusic(bool isMuted) {
            IsMusicMuted = isMuted;
            PersistenceManager.Instance.IsMusicMuted = isMuted;
            musicSource.mute = IsMusicMuted;
        }

        public void SetMuteForSfx(bool isMuted) {
            IsSoundMuted = isMuted;
            PersistenceManager.Instance.IsSfxMuted = isMuted;

            for (int indEfxSource = 0; indEfxSource < m_sfxSources.Length; indEfxSource++)
                m_sfxSources[indEfxSource].mute = IsSoundMuted;

            for (int indEfxSource = 0; indEfxSource < m_sfxCoinsSources.Length; indEfxSource++)
                m_sfxCoinsSources[indEfxSource].mute = IsSoundMuted;
        }
        #endregion

        #region Helpers
        private AudioSource[] InitAudioSources(int numAudioSources) {
            AudioSource[] audioSources = new AudioSource[numAudioSources];

            for(int indAudioSource = 0; indAudioSource < audioSources.Length; indAudioSource++) {
                AudioSource audioSource = gameObject.AddComponent<AudioSource>();
                audioSource.volume = 0.5f;
                audioSource.playOnAwake = false;

                audioSources[indAudioSource] = audioSource;
            }

            return audioSources;
        }

        private void PlaySingle(AudioClip clip, float delay = 0.0f, bool isRandomPitch = false) {
            AudioSource[] audioSources = null;

            if(clip.name == COIN_SFX)
                audioSources = m_sfxCoinsSources;
            else
                audioSources = m_sfxSources;

            AudioSource efxSource = null;

            for(int indEfxSource = 0; indEfxSource < audioSources.Length; indEfxSource++) {
                efxSource = audioSources[indEfxSource];

                if(!efxSource.isPlaying) {
                    SetPitch(efxSource, isRandomPitch);
                    efxSource.clip = clip;
                    efxSource.PlayDelayed(delay);
                    break;
                }
            }

            //print("Not enought efx sources! for => " + clip.name);

            if(clip.name == APPLAUSE_SFX) {//applause sfx is important, play it anyway
                efxSource = audioSources[Random.Range(0, audioSources.Length)];
                SetPitch(efxSource, isRandomPitch);
                efxSource.clip = clip;
                efxSource.PlayDelayed(delay);
            }
        }

        private void SetPitch(AudioSource audioSource, bool isRandomPitch) {
            if(isRandomPitch)
                audioSource.pitch = Random.Range(0.7f, 1.3f);
            else
                audioSource.pitch = 1.0f;
        }
        #endregion
    }
}
﻿namespace AIdle {
    public class Constants {
        public class SceneNames {
            public const string LOADER = "Loader";
            public const string MENU = "Menu";
            public const string GAME = "Game";
        }

        public class Settings {
            public const int TILE_START_MAX_HEALTH = 7;
            public const int CHEST_CHANCE = 100;//percents
            public const int MAX_VISUAL_THROWN_COINS__CHANGES = 4;
            public const int STONES_CHANCE = 30;//percents
            public const int COINS_TILE_NORMAL = 1;
            public const int COINS_TILE_CHEST = 5;
        }

        public class PlayerPrefs {
            public const string IS_MUSIC_MUTED = "IS_MUSIC_MUTED";
            public const string IS_SFX_MUTED = "IS_SFX_MUTED";
        }
    }
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace AIdle {
    public class GameSceneUI : MonoBehaviour {
        #region Fields
        [SerializeField]
        private NewItemPanel m_newItemPanel;
        [SerializeField]
        private ItemsPanel m_itemsPanel;
        [SerializeField]
        private ToolInfoView m_pickaxeInfoView;
        [SerializeField]
        private ToolInfoView m_workerInfoView;
        public TileHealthLabel tileHealthLabel;
        #endregion

        public Transform m_gnome;
        public GameObject m_mainCoin;
        public ObjectPool m_coinsPool;
        [SerializeField]
        private GameObject m_itemInNewItemPanel;
        private Image m_musicButtonImage;
        private Image m_soundButtonImage;

        #region Public
        public void ShowNewItem(ItemData itemData) {
            SoundManager.Instance.PlayApplauseSound();
            m_newItemPanel.ShowItem(itemData);
        }

        public void ShowItemsPanel() {
            m_itemsPanel.Open();
        }

        public void ThrowCoin(Vector3 from, float timeToMove, float timeToFade) {
            SoundManager.Instance.PlayCoinSound();

            GameObject coin = m_coinsPool.GetObject(timeToMove);
            coin.transform.position = from;
            coin.transform.DOMove(m_mainCoin.transform.position, timeToMove, false);
            Image coinImage = coin.GetComponent<Image>();
            coinImage.color = Color.white;
            coinImage.DOFade(0.0f, timeToFade).SetEase(Ease.InExpo);
        }
        #endregion

        #region Unity Events
        private void Start() {
            m_pickaxeInfoView.Init(ResourcesManager.Instance.Pickaxe);
            m_workerInfoView.Init(ResourcesManager.Instance.PassiveIncome);
        }
        #endregion
    }
}
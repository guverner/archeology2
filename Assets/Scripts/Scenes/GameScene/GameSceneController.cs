﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AIdle {
    public class GameSceneController : UnitySingleton<GameSceneController> {
        #region Fields
        public GameSceneAssets gameSceneAssets;
        public GameSceneUI ui;

        public Tile[] m_tiles;

        public ObjectPool grassPool;
        public ObjectPool earthPool;
        public ObjectPool stonePool;

        private Coroutine m_passiveIncomeCoroutine;
        #endregion

        public int NumChestsOnBoard { get; set; }

        #region Unity Events
        private void OnDestroy() {
            if(ResourcesManager.IsInstantiated)
                ResourcesManager.Instance.onUpgradePassiveIncome -= OnUpgradePassiveIncome;
        }
        #endregion

        #region Initialisation
        protected override void Init() {
            TileInfo[] tilesInfo = ResourcesManager.Instance.TilesInfos;

            if (tilesInfo == null) {
                tilesInfo = new TileInfo[m_tiles.Length];
                int tileMaxHealth = Constants.Settings.TILE_START_MAX_HEALTH;

                for (int i = 0; i < tilesInfo.Length; i++) {
                    int health = Random.Range(tileMaxHealth - 3, tileMaxHealth);
                    tilesInfo[i] = new TileInfo(health, health, TileInfo.TileType.GRASS);
                }
                ResourcesManager.Instance.SetTilesInfo(tilesInfo);
            }

            InitTiles(m_tiles, tilesInfo);

            ResourcesManager.Instance.onUpgradePassiveIncome += OnUpgradePassiveIncome;

            if (ResourcesManager.Instance.PassiveIncome.Level > 0)
                m_passiveIncomeCoroutine = StartCoroutine(PassiveIncomeCoroutine(1.0f));
        }

        private void OnUpgradePassiveIncome(UpgradableTool passiveIncomeTool) {
            if(m_passiveIncomeCoroutine != null)
                StopCoroutine(m_passiveIncomeCoroutine);
            m_passiveIncomeCoroutine = StartCoroutine(PassiveIncomeCoroutine());
        }
        #endregion

        #region Tiles
        private void InitTiles(Tile[] tilesArray, TileInfo[] tilesInfo) {
            for (int i = 0; i < tilesArray.Length; i++) {
                TileInfo tileInfo = tilesInfo[i];
                tilesArray[i].Init(i, tilesInfo[i]);
                if (tileInfo.type == TileInfo.TileType.CHEST)
                    NumChestsOnBoard++;
            }
        }
        #endregion

        #region Helpers
        private IEnumerator PassiveIncomeCoroutine(float delayBeforeSec = 0.0f) {
            if(delayBeforeSec > 0.0f)
                yield return new WaitForSeconds(delayBeforeSec);

            int numCoinsThrow = (int)ResourcesManager.Instance.PassiveIncome.Value;
            if (numCoinsThrow > 4) numCoinsThrow = 4;

            while (true) {
                for (int i = 0; i < numCoinsThrow; i++)
                    ui.ThrowCoin(GetRandomPosFromGnome(), 1.0f, 1.0f);
                yield return new WaitForSeconds(1.0f);
                ResourcesManager.Instance.AddCoinsByPassiveIncome();
            }
        }

        private Vector2 GetRandomPosFromGnome() {
            Vector2 gnomePos = ui.m_gnome.position;
            gnomePos.x += Random.Range(-40, 41);
            gnomePos.y += Random.Range(-40, 21);
            return gnomePos;
        }
        #endregion
    }
}
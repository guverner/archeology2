﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIdle {
    public class PickaxeInfoView : ToolInfoView {
        public override void Init(UpgradableTool upgradableTool) {
            m_valuePrefix = LocaleManager.Instance.Text("DAMAGE") + ": ";
            base.Init(upgradableTool);
        }

        protected override void UpdateView() {
            base.UpdateView();
            m_valueLabel.text = m_valuePrefix + m_upgradableTool.Value;
        }
    }
}
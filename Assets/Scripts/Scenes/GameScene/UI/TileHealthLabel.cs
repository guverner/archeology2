﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AIdle {
    public class TileHealthLabel : MonoBehaviour {
        public Text label;

        public void Show(TileInfo tileInfo) {
            label.text = tileInfo.currentHealth + "/" + tileInfo.maxHealth;
            label.enabled = true;
        }

        public void Hide() {
            label.enabled = false;
        }
    }
}
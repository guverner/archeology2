﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace AIdle {
    public class CoinsContainer : MonoBehaviour {
        #region Fields
        [SerializeField]
        private RectTransform m_coinGO;
        [SerializeField]
        private Text m_label;
        #endregion

        #region Unity Events
        private void Start() {
            m_label.text = ((int)ResourcesManager.Instance.Coins).ToString();
            ResourcesManager.Instance.onCoinsChanged += OnCoinsChanged;
        }

        private void OnDestroy() {
            if(ResourcesManager.IsInstantiated)
                ResourcesManager.Instance.onCoinsChanged -= OnCoinsChanged;
        }
        #endregion

        #region Helpers
        private void OnCoinsChanged(float changedBy, float numCoinsTotal) {
            m_label.text = ((int)numCoinsTotal).ToString();
            m_coinGO.DOScale(new Vector3(1.3f, 1.3f, 1.0f), 0.2f);
            m_coinGO.DOScale(new Vector3(1.0f, 1.0f, 1.0f), 0.3f).SetDelay(0.2f);
        }
        #endregion
    }
}
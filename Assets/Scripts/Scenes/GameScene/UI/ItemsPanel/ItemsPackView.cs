﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AIdle {
    public class ItemsPackView : MonoBehaviour {
        #region Fields
        public Dictionary<string, Transform> itemsGoDict;
        #endregion

        #region Public
        public void Init() {
            itemsGoDict = new Dictionary<string, Transform>(this.transform.childCount);

            foreach (Transform child in this.transform)
                itemsGoDict.Add(child.name, child);
        }

        public void MakeItemShown(string itemID) {
            Text nameLabel = itemsGoDict[itemID].GetChild(1).GetComponent<Text>();
            ItemData itemData = ItemsManager.Instance.GetItem(itemID);

            nameLabel.text = LocaleManager.Instance.Text(itemData.name);

            if (itemsGoDict.Count > 1) {//because of one item is set to normal on the scene
                Transform itemImageGo = itemsGoDict[itemID].GetChild(0);
                itemImageGo.GetComponent<Image>().color = Color.white;
                itemImageGo.GetComponent<Shadow>().enabled = true;
                nameLabel.enabled = true;
            }
        }

        public void SetPosition(float posX) {
            this.GetComponent<RectTransform>().anchoredPosition = new Vector2(posX, 0.0f);
        }

        public void Move(float fromX, float toX, float moveTime) {
            this.gameObject.SetActive(true);
            this.GetComponent<RectTransform>().anchoredPosition = new Vector2(fromX, 0.0f);
            StartCoroutine(MoveToCoroutine(toX, moveTime));
        }
        #endregion

        #region Helpers
        private IEnumerator MoveToCoroutine(float moveByX, float moveTime) {
            RectTransform rt = this.GetComponent<RectTransform>();
            Vector2 startPosition = rt.anchoredPosition;
            Vector2 finalPosition = new Vector2(startPosition.x + moveByX, startPosition.y);

            float currentTime = 0.0f;

            while (currentTime < moveTime) {
                currentTime += Time.deltaTime;
                rt.anchoredPosition = Vector2.Lerp(startPosition, finalPosition, currentTime / moveTime);
                yield return null;
            }
        }
        #endregion
    }
}
﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace AIdle {
    public class ItemsPanel : MonoBehaviour {
        #region Fields
        #region UI Fields
        public CButtonWithOff soundButton;
        public CButtonWithOff musicButton;
        
        public Button closeButton;
        public Text numItemsLabel;
        public GameObject dontHaveItemsLabel;
        public ButtonPointerHandler leftArrowButton;
        public ButtonPointerHandler rightArrowButton;
        public Transform itemsRect;//rectangle with mask
        #endregion

        private ItemsPackView[] m_itemViews;
        private List<int> m_itemsIndexesPlayerHave;//belongs to m_itemViews
        private int m_currentItemIndex;//belongs to m_indexesPlayerHave
        private bool m_isArrowButtonsBlocked;
        private Coroutine m_moveCoroutine;
        #endregion

        #region Unity Events
        private void Awake() {
            soundButton.Init(!SoundManager.Instance.IsSoundMuted, OnSoundClick);
            musicButton.Init(!SoundManager.Instance.IsMusicMuted, OnMusicClick);
            closeButton.onClick.AddListener(OnCloseClick);
            leftArrowButton.SetAfterClickCallback(LeftArrowButtonClick);
            rightArrowButton.SetAfterClickCallback(RightArrowButtonClick);

            m_itemViews = new ItemsPackView[itemsRect.childCount];

            int i = 0;
            foreach (Transform child in itemsRect) {
                ItemsPackView itemsPackView = child.GetComponent<ItemsPackView>();
                itemsPackView.Init();
                m_itemViews[i] = itemsPackView;
                i++;
            }
        }

        private void OnEnable() {
            List<string> itemsPlayerHave = ItemsManager.Instance.ItemsPlayerHave;

            m_itemsIndexesPlayerHave = new List<int>();

            foreach (string itemID in itemsPlayerHave) {
                int ind = 0;
                foreach (ItemsPackView itemsPackView in m_itemViews) {
                    if (itemsPackView.itemsGoDict.ContainsKey(itemID)) {
                        if(!m_itemsIndexesPlayerHave.Contains(ind))
                            m_itemsIndexesPlayerHave.Add(ind);
                        itemsPackView.MakeItemShown(itemID);
                        break;
                    }
                    ind++;
                }
            }

            foreach (ItemsPackView itemsPackView in m_itemViews)
                itemsPackView.gameObject.SetActive(false);

            m_currentItemIndex = 0;

            //show first item pack
            if (m_itemsIndexesPlayerHave.Count > 0) {
                ItemsPackView itemsPackView = m_itemViews[m_itemsIndexesPlayerHave[m_currentItemIndex]];
                itemsPackView.SetPosition(0);
                itemsPackView.gameObject.SetActive(true);
            }

            UpdateNumItems();
            UpdateArrowButtons();
        }
        #endregion

        #region Public
        public void Open() {
            this.gameObject.SetActive(true);
        }
        #endregion

        #region Button Handlers
        private void OnSoundClick(bool isOn) {
            SoundManager.Instance.SetMuteForSfx(!isOn);
        }

        private void OnMusicClick(bool isOn) {
            SoundManager.Instance.SetMuteForMusic(!isOn);
        }

        private void OnCloseClick() {
            SoundManager.Instance.PlayButtonSound();
            this.gameObject.SetActive(false);
        }

        public void LeftArrowButtonClick() {
            if (m_isArrowButtonsBlocked) return;

            m_isArrowButtonsBlocked = true;

            SoundManager.Instance.PlayButtonSound();

            m_currentItemIndex--;
            UpdateArrowButtons(m_currentItemIndex);

            if (m_moveCoroutine != null)
                StopCoroutine(m_moveCoroutine);
            m_moveCoroutine = StartCoroutine(ChangeItemShownCoroutine(m_currentItemIndex + 1, m_currentItemIndex));
        }

        public void RightArrowButtonClick() {
            if (m_isArrowButtonsBlocked) return;

            m_isArrowButtonsBlocked = true;

            SoundManager.Instance.PlayButtonSound();

            m_currentItemIndex++;
            UpdateArrowButtons(m_currentItemIndex);

            if (m_moveCoroutine != null)
                StopCoroutine(m_moveCoroutine);
            m_moveCoroutine = StartCoroutine(ChangeItemShownCoroutine(m_currentItemIndex - 1, m_currentItemIndex));
        }
        #endregion

        #region Helpers
        private void UpdateArrowButtons(int indOpenedItem = 0) {
            int numItemsPlayerHave = ItemsManager.Instance.ItemsPlayerHave.Count;

            if (numItemsPlayerHave == 0) {
                leftArrowButton.gameObject.SetActive(false);
                rightArrowButton.gameObject.SetActive(false);
            }
            else {
                leftArrowButton.gameObject.SetActive(indOpenedItem != 0);
                rightArrowButton.gameObject.SetActive(indOpenedItem != m_itemsIndexesPlayerHave.Count - 1);
            }
        }

        private void UpdateNumItems() {
            int numItemsPlayerHave = ItemsManager.Instance.ItemsPlayerHave.Count;

            dontHaveItemsLabel.SetActive(numItemsPlayerHave == 0);
            numItemsLabel.text = numItemsPlayerHave + "/" + ItemsManager.Instance.NumItemsTotal;
        }

        private IEnumerator ChangeItemShownCoroutine(int lastIndex, int currentIndex) {
            const float MOVE_TIME = 0.5f;
            float MOVE_BY = 768.0f;

            if (lastIndex > currentIndex)
                MOVE_BY *= -1.0f;

            ItemsPackView lastItemsPackView = m_itemViews[m_itemsIndexesPlayerHave[lastIndex]];
            lastItemsPackView.Move(0, -MOVE_BY, MOVE_TIME);

            ItemsPackView currentItemsPackView = m_itemViews[m_itemsIndexesPlayerHave[currentIndex]];
            currentItemsPackView.SetPosition(MOVE_BY);
            currentItemsPackView.Move(MOVE_BY, - MOVE_BY, MOVE_TIME);

            yield return new WaitForSeconds(MOVE_TIME);

            lastItemsPackView.gameObject.SetActive(false);
            m_isArrowButtonsBlocked = false;
        }
        #endregion
    }
}
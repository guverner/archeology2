﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

namespace AIdle {
    public class CButtonWithOff : ButtonPointerHandler {
        #region Fields
        #region UI Fields
        [SerializeField]
        private Image m_spriteOff;
        #endregion

        private bool m_isOn = true;
        private UnityAction<bool> m_clickCallback;
        #endregion

        public void Init(bool isOn, UnityAction<bool> clickCallback) {
            m_clickCallback = clickCallback;
            SetIsOn(isOn);
        }

        protected override void OnAfterClick() {
            SoundManager.Instance.PlayButtonSound();
            SetIsOn(!m_isOn);
            if (m_clickCallback != null)
                m_clickCallback(m_isOn);
        }

        public void SetIsOn(bool isOn) {
            m_isOn = isOn;
            m_spriteOff.enabled = !isOn;
        }
    }
}
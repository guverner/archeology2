﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace AIdle {
    public class NewItemPanel : MonoBehaviour {
        #region Inspector Fields
        [SerializeField]
        private CanvasGroup m_canvasGroup;
        [SerializeField]
        private Image m_itemSprite;
        [SerializeField]
        private Text m_itemLabel;
        [SerializeField]
        private Transform m_itemContainer;
        #endregion

        #region Public
        public void ShowItem(ItemData item) {
            const float START_SCALE_VAL = 0.5f;

            m_canvasGroup.alpha = 1.0f;
            m_itemContainer.localScale = new Vector3(START_SCALE_VAL, START_SCALE_VAL, 1.0f);

            m_itemSprite.sprite = GameSceneController.Instance.gameSceneAssets.GetItemSprite(item.id);
            m_itemLabel.text = LocaleManager.Instance.Text(item.name);

            this.gameObject.SetActive(true);

            Sequence mySequence = DOTween.Sequence();

            const float SCALE_VAL = 1.0f;
            const float IN_DURATION = 1.3f;
            const float SHOW_DURATION = 2.0f;
            const float OUT_DURATION = 0.3f;

            mySequence.Append(m_itemContainer.DOScale(
                new Vector3(SCALE_VAL, SCALE_VAL, 1.0f), IN_DURATION).SetEase(Ease.OutBounce));
            mySequence.AppendInterval(SHOW_DURATION);
            mySequence.Append(m_canvasGroup.DOFade(0.0f, OUT_DURATION));
            mySequence.AppendCallback(Hide);
        }
        #endregion

        #region Helpers
        private void Hide() {
            this.gameObject.SetActive(false);
        }
        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIdle {
    public class WorkerInfoView : ToolInfoView {
        public override void Init(UpgradableTool upgradableTool) {
            m_valuePrefix = LocaleManager.Instance.Text("MINING");
            base.Init(upgradableTool);
        }

        protected override void UpdateView() {
            base.UpdateView();
            m_valueLabel.text = string.Format(m_valuePrefix, m_upgradableTool.Value);
        }
    }
}
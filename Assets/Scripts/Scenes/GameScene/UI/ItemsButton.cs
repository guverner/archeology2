﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIdle {
    public class ItemsButton : ButtonPointerHandler {
        protected override void OnAfterClick() {
            SoundManager.Instance.PlayButtonSound();
            GameSceneController.Instance.ui.ShowItemsPanel();
        }
    }
}
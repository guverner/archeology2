﻿using UnityEngine;
using UnityEngine.UI;

namespace AIdle {
    public class ToolInfoView : MonoBehaviour {
        #region Fields
        #region UI Fields
        [SerializeField]
        protected Text m_valueLabel;
        [SerializeField]
        protected Text m_priceLabel;
        [SerializeField]
        protected Button m_upgradeButton;
        [SerializeField]
        protected Color m_canUpgradeColor;
        [SerializeField]
        protected Color m_canNotUpgradeColor;
        #endregion

        protected UpgradableTool m_upgradableTool;
        protected string m_valuePrefix;
        #endregion

        #region Unity Events
        private void OnDestroy() {
            if (ResourcesManager.IsInstantiated)
                ResourcesManager.Instance.onCoinsChanged -= OnCoinsChanged;
        }
        #endregion

        public virtual void Init(UpgradableTool upgradableTool) {
            m_upgradableTool = upgradableTool;
            ResourcesManager.Instance.onCoinsChanged += OnCoinsChanged;
            m_upgradeButton.onClick.AddListener(OnUpdateButtonClick);
            UpdateView();
        }

        protected void SetCanUpgrade(bool isCanUpgrade) {
            if (isCanUpgrade)
                m_priceLabel.color = m_canUpgradeColor;
            else
                m_priceLabel.color = m_canNotUpgradeColor;
        }

        private void OnCoinsChanged(float changedBy, float numCoinsTotal) {
            SetCanUpgrade(numCoinsTotal >= m_upgradableTool.Price());
        }

        protected virtual void UpdateView() {
            SetCanUpgrade(ResourcesManager.Instance.Coins >= m_upgradableTool.Price());
            m_priceLabel.text = m_upgradableTool.Price().ToString();
        }

        private void OnUpdateButtonClick() {
            SoundManager.Instance.PlayButtonSound();
            if (m_upgradableTool.Buy())
                UpdateView();
            else
                Debug.Log("Not enought res");
        }
    }
}
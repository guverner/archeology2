﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AIdle {
    public class ObjectPool : MonoBehaviour {
        #region Fields
        [SerializeField]
        public int m_numObjectsAtStart = 5;
        [SerializeField]
        public GameObject m_mainObjectPrefab;
        [SerializeField]
        public float m_timeToLiveSec = 1.0f;
        [SerializeField]
        public bool m_isHaveMaxNum = false;
        [SerializeField]
        public int m_maxNum = 5;

        public List<GameObject> m_availableObjects;
        private int m_numObjectsTotal;
        #endregion

        #region Unity Events
        private void Awake() {
            if (m_mainObjectPrefab == null) return;

            m_availableObjects = new List<GameObject>();

            if (m_isHaveMaxNum && m_maxNum < m_numObjectsAtStart)
                m_numObjectsAtStart = m_maxNum;

            CreateNewObjects(m_numObjectsAtStart);
        }
        #endregion

        #region Public
        public GameObject GetObject(float timeToLiveSec = -1.0f) {
            if (m_availableObjects.Count == 0 && (!m_isHaveMaxNum || m_numObjectsTotal <= m_maxNum)) {
                int numObjectsCreate = 5;

                if (m_isHaveMaxNum && (m_numObjectsTotal + numObjectsCreate) > m_maxNum)
                    numObjectsCreate = m_maxNum - m_numObjectsTotal;

                CreateNewObjects(numObjectsCreate);
            }

            if (m_availableObjects.Count > 0) {
                if (timeToLiveSec <= 0.0f)
                    timeToLiveSec = m_timeToLiveSec;

                GameObject obj = m_availableObjects[0];
                obj.SetActive(true);
                m_availableObjects.RemoveAt(0);
                StartCoroutine(MakeObjectAvailableCoroutine(obj, timeToLiveSec));

                return obj;
            }
            else {
                return null;
            }
        }
        #endregion

        #region Helpers
        private void CreateNewObjects(int numObjectsCreate) {
            if (numObjectsCreate <= 0) return;

            m_numObjectsTotal += numObjectsCreate;

            for (int indObject = 0; indObject < numObjectsCreate; indObject++)
                m_availableObjects.Add(InitObject());
        }

        private GameObject InitObject() {
            GameObject obj = GameObject.Instantiate(m_mainObjectPrefab, this.transform, false);
            obj.SetActive(false);
            return obj;
        }

        private IEnumerator MakeObjectAvailableCoroutine(GameObject obj, float timeToLiveSec) {
            if (timeToLiveSec > 0.0f)
                yield return new WaitForSeconds(timeToLiveSec);

            Color newColor = obj.GetComponent<Image>().color;
            newColor.a = 1.0f;
            obj.GetComponent<Image>().color = newColor;
            obj.SetActive(false);

            m_availableObjects.Add(obj);
        }
        #endregion
    }
}
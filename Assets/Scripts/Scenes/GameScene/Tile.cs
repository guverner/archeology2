﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace AIdle {
    public class Tile : ButtonPointerHandler {
        #region Fields
        #region UI
        [SerializeField]
        private Image m_tileImage;
        [SerializeField]
        private Image m_tileOverImage;
        [SerializeField]
        private Sprite[] m_tilesSprites;
        [SerializeField]
        private Sprite[] m_tilesOverSprites;
        #endregion
        
        private int m_index;
        private TileInfo m_tileInfo;
        #endregion

        #region Public
        public void Init(int index, TileInfo tileInfo) {
            m_index = index;
            m_tileInfo = tileInfo;
            ChangeTileType(tileInfo.type);
            SetTileOverOpacity(tileInfo, true);
        }
        #endregion

        #region IPointer
        protected override void OnAfterClick() {
            SoundManager.Instance.PlayShovelSound();
            ThrowWasteObject(m_tileInfo.type, Input.mousePosition, 0.5f);

            m_tileInfo.RemoveHealth((int)ResourcesManager.Instance.Pickaxe.Value);

            if (m_tileInfo.currentHealth == 0)
                Death(m_tileInfo.type);
            else
                SetTileOverOpacity(m_tileInfo, false);

            PersistenceManager.Instance.Save();
        }

        protected override void OnDown() {
            this.transform.SetAsLastSibling();
            GameSceneController.Instance.ui.tileHealthLabel.Show(m_tileInfo);
        }

        protected override void OnUp() {
            GameSceneController.Instance.ui.tileHealthLabel.Hide();
        }
        #endregion

        #region Helpers
        private void ChangeTileType(TileInfo.TileType newTileType) {
            m_tileImage.sprite = m_tilesSprites[(int)newTileType];

            Sprite newOverSprite = m_tilesOverSprites[(int)newTileType];

            if (newOverSprite != null) {
                m_tileOverImage.enabled = true;
                m_tileOverImage.sprite = newOverSprite;
            }
            else {
                m_tileOverImage.enabled = false;
            }
        }

        private void ThrowWasteObject(TileInfo.TileType tileType, Vector3 position, float time = 1.0f) {
            GameObject wasteParticle = null;

            switch(tileType) {
                case TileInfo.TileType.GRASS:
                    wasteParticle = GameSceneController.Instance.grassPool.GetObject(time);
                    break;
                case TileInfo.TileType.STONE:
                    wasteParticle = GameSceneController.Instance.stonePool.GetObject(time);
                    break;
                default:
                    wasteParticle = GameSceneController.Instance.earthPool.GetObject(time);
                    break;
            }

            if(wasteParticle != null) {
                wasteParticle.transform.position = position;
                wasteParticle.transform.Rotate(0.0f, 0.0f, Random.Range(-15.0f, 15.0f));
                wasteParticle.transform.DOBlendableMoveBy(new Vector3(Random.Range(-20, 20), 55.0f, 0.0f), time);
            }
        }

        private void SetTileOverOpacity(TileInfo tileInfo, bool isAtStart) {
            if (!m_tileOverImage.enabled) return;

            float alpha = (float)tileInfo.currentHealth / (float)tileInfo.maxHealth;
            if (tileInfo.type != TileInfo.TileType.CHEST)
                alpha = 1.0f - alpha;

            Color newColor = m_tileOverImage.color;
            newColor.a = alpha;
            m_tileOverImage.color = newColor;
        }

        private void Death(TileInfo.TileType type) {
            int numCoinsToGive = Constants.Settings.COINS_TILE_NORMAL;

            GameSceneController.Instance.ui.ThrowCoin(transform.position, 1.0f, 0.8f);

            IsBlocked = true;

            if (type == TileInfo.TileType.CHEST) {
                GameSceneController.Instance.NumChestsOnBoard--;

                numCoinsToGive = Constants.Settings.COINS_TILE_CHEST;

                ItemData[] addedItems = ItemsManager.Instance.AddRandomItem();

                if (addedItems[0] != null)
                    GameSceneController.Instance.ui.ShowNewItem(addedItems[0]);
            }

            ResourcesManager.Instance.AddCoins(numCoinsToGive);

            this.transform.DORotate(new Vector3(0.0f, 0.0f, -360.0f), 
                0.3f, RotateMode.FastBeyond360).OnComplete(ResetRotation);

            ChangeTileToNew();
        }

        private int GenerateNextTileHealth() {
            return (int)Random.Range(7, 16);
        }

        private TileInfo.TileType GenerateNextTileType() {
            TileInfo.TileType nextTileType = TileInfo.TileType.EARTH;

            int numItemsCanFall = ItemsManager.Instance.GetNumItemsCanFall();

            if (numItemsCanFall > 0 &&
                Random.Range(0, 100) < Constants.Settings.CHEST_CHANCE &&
                GameSceneController.Instance.NumChestsOnBoard < numItemsCanFall) {

                nextTileType = TileInfo.TileType.CHEST;
                GameSceneController.Instance.NumChestsOnBoard++;
            }
            else if (Random.Range(0, 100) < Constants.Settings.STONES_CHANCE) {
                nextTileType = TileInfo.TileType.STONE;
            }

            return nextTileType;
        }

        private void ResetRotation() {
            this.GetComponent<RectTransform>().rotation = Quaternion.identity;
            IsBlocked = false;
        }

        private void ChangeTileToNew() {
            m_tileInfo.type = GenerateNextTileType();
            m_tileInfo.maxHealth = GenerateNextTileHealth();
            m_tileInfo.currentHealth = m_tileInfo.maxHealth;

            ChangeTileType(m_tileInfo.type);
            SetTileOverOpacity(m_tileInfo, true);
        }
        #endregion
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIdle {
    public class GameSceneAssets : MonoBehaviour {
        public Sprite[] itemSprites;

        public Sprite GetItemSprite(string itemID) {
            foreach (Sprite spr in itemSprites) {
                if (spr.name == itemID)
                    return spr;
            }
            return null;
        }
    }
}
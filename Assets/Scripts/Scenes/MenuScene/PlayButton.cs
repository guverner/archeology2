﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AIdle {
    public class PlayButton : ButtonPointerHandler {
        protected override void OnAfterClick() {
            SoundManager.Instance.PlayButtonSound();
            GameManager.Instance.LoadGameScene();
        }
    }
}
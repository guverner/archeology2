﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace AIdle {
    public class LoadingScreen : MonoBehaviour {
        #region Fields
        [SerializeField]
        private CanvasGroup m_loadingContainer;
        [SerializeField]
        private Transform m_loaderImage;
        [SerializeField]
        private float m_fadeInDuration;//in seconds
        [SerializeField]
        private float m_fadeOutDuration;//in seconds
        [SerializeField]
        private float m_minTimeShow;//in seconds

        private bool m_isFadeInProcess;
        private UnityAction<bool> m_onFadeComplete;
        private System.DateTime m_startShowTime;
        private bool m_isHideCalled;
        private bool m_isCanHide;
        #endregion

        #region Properties
        public bool IsShown { get; private set; }
        #endregion

        #region Unity Events
        private void Awake() {
            DontDestroyOnLoad(gameObject.transform.parent.gameObject);
        }
        #endregion

        #region Public
        public void Init(UnityAction<bool> onFadeCompleteCallback) {
            m_onFadeComplete = onFadeCompleteCallback;
        }

        public void Show(bool isCanHide) {
            if(IsShown || m_isFadeInProcess) return;

            m_isHideCalled = false;
            m_isCanHide = isCanHide;
            m_startShowTime = System.DateTime.Now;
            IsShown = true;
            gameObject.SetActive(true);
            Fade(true, m_fadeInDuration);
        }

        public void Hide() {
            if(!m_isCanHide) {
                m_isHideCalled = true;
                return;
            }
            else if(!IsShown || m_isFadeInProcess) {
                return;
            }

            m_isHideCalled = true;
            Fade(false, m_fadeOutDuration);
        }

        public void MakeAbleToHide() {
            m_isCanHide = true;

            if (m_isHideCalled)
                Hide();
        }
        #endregion

        #region Helpers
        private void Fade(bool isIn, float durationSec) {
            m_isFadeInProcess = true;

            float endValue = isIn == true ? 1.0f : 0.0f;

            Tween tween = DOTween.To(() => m_loadingContainer.alpha, x => m_loadingContainer.alpha = x,
                endValue, durationSec);

            if(isIn) {
                tween.OnComplete(OnFadeInComplete);
            }
            else {
                float elapsedSec = (float)(System.DateTime.Now - m_startShowTime).TotalSeconds;
                if (elapsedSec < m_minTimeShow)
                    tween.SetDelay(m_minTimeShow - elapsedSec);
                tween.OnComplete(OnFadeOutComplete);
            }
        }

        private void OnFadeInComplete() {
            m_isFadeInProcess = false;

            m_loaderImage.DORotate(new Vector3(0.0f, 0.0f, -360.0f),
                1.5f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental);
            if (m_onFadeComplete != null)
                m_onFadeComplete(true);
        }

        private void OnFadeOutComplete() {
            IsShown = false;
            m_isFadeInProcess = false;

            gameObject.SetActive(false);
            m_loaderImage.DOKill(true);
            m_loaderImage.rotation = Quaternion.identity;
            if (m_onFadeComplete != null)
                m_onFadeComplete(false);
        }
        #endregion
    }
}
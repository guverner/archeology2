﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace AIdle {
    public class XMLToBinaryMenuItem {
        private const string XML_BIN_FILE_POSTFIX = "Binary";

        #region Item
        [MenuItem("Assets/XMLToBinary/Item")]
        private static void CreateItem() {
            if (Selection.activeObject != null)
                CreateBinaryFileFromXml<ItemDataCollection>();
        }

        [MenuItem("Assets/XMLToBinary/Item", true)]
        private static bool ValidationItem() {
            return IsSelectedObjIsXmlAsset();
        }
        #endregion

        #region Locale
        [MenuItem("Assets/XMLToBinary/Locale")]
        private static void CreateLocale() {
            if(Selection.activeObject != null)
                CreateBinaryFileFromXml<LocaleDataCollection>();
        }

        [MenuItem("Assets/XMLToBinary/Locale", true)]
        private static bool ValidationLocale() {
            return IsSelectedObjIsXmlAsset();
        }
        #endregion

        #region Helpers
        private static void CreateBinaryFileFromXml<T>() {
            string pathInUnityProject = AssetDatabase.GetAssetPath(Selection.activeObject);
            string parentDirectoryPath = Path.GetDirectoryName(Path.GetFullPath(pathInUnityProject));
            string fullDirectoryAndFilenamePath =
                (parentDirectoryPath + "/" + Path.GetFileName(Selection.activeObject.name)).Replace("\\", "/");

            T obj = XMLManager.Read<T>(fullDirectoryAndFilenamePath, false, true);
            BinaryFileManager.Save(obj, fullDirectoryAndFilenamePath + XML_BIN_FILE_POSTFIX, false, true);
        }

        private static bool IsSelectedObjIsXmlAsset() {
            Object selectedObj = Selection.activeObject;

            return
                selectedObj != null &&
                selectedObj.GetType() == typeof(TextAsset) &&
                AssetDatabase.Contains(selectedObj) && //Returns true when an object is an asset (corresponds to a file in the Assets folder)
                Path.GetExtension(AssetDatabase.GetAssetPath(selectedObj)) == ".xml";
        }
        #endregion
    }
}
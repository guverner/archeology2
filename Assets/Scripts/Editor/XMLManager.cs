﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace AIdle {
    public class XMLManager {
        private const string EXTENSION = ".xml";

        public static T Read<T>(string path, 
            bool isPersistentDataPath = false, bool isForcePath = false) {

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (XmlReader reader = XmlReader.Create(CreatePath(path, isForcePath, isPersistentDataPath))) {
                return (T)serializer.Deserialize(reader);
            }
        }

        public static void Write<T>(T obj, string path, 
            bool isPersistentDataPath = false, bool isForcePath = false) {

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (XmlWriter writer = XmlWriter.Create(CreatePath(path, isForcePath, isPersistentDataPath))) {
                serializer.Serialize(writer, obj);
            }
        }

        private static string CreatePath(string path, bool isForcePath, bool isPersistentDataPath) {
            System.Text.StringBuilder fullPathSB = new System.Text.StringBuilder();
            if (!isForcePath) {
                fullPathSB.Append(isPersistentDataPath == true ? Application.persistentDataPath : Application.dataPath);
                fullPathSB.Append("/");
            }
            fullPathSB.Append(path + EXTENSION);
            return fullPathSB.ToString();
        }
    }
}